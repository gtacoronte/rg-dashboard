<html>
<head>
<meta name="viewport" content="width=device-width" />
<script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
<link rel="stylesheet" href="//ajax.googleapis.com/ajax/libs/jqueryui/1.11.1/themes/smoothness/jquery-ui.css" />
<script src="//ajax.googleapis.com/ajax/libs/jqueryui/1.11.1/jquery-ui.min.js"></script>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.3.0/css/font-awesome.css">

<?php
$day = date('Y-m-d');

?>

<style>
    body {
        font-family: arial;
    }
    .error {
        border: 2px solid red;
    }
    input {
        padding: 4px 8px;
        font-size: 16px;
        margin: 0px 20px 20px 10px;
    }
    
    .input {
        display: inline-block;
    }
    .input label {
        display: block;
        margin: 0px 20px 10px 10px;
    }
    
    .submit_button {
        width: 60px;
        font-size: 16px;
        height: 30px;
    }
    
    .waiting {
        display: none;
        width: 100%;
        height: 100px;
        text-align: center;
        font-size: 25px;
        background-color: #BBB;
    }
    
    .waiting p {
        transform: translateY(150%);
    }
    
    .results {
        width: 100%;
    }
    
    .last_updated {
        font-weight: bold;
        margin: 10px !important;
    }
   
    @media screen and (max-width: 599px)  {
        
        .input {
            display: block;
            width: 90%;
        }
        
        .submit_button {
            max-width: 350px;
            width: 90%;
            font-size: 16px;
            margin: 0px 0px 20px 10px;
            height: 40px;
        }
        
        input {
            max-width: 350px;
            width: 100%;
        }
        .last_updated {
            display: block;
            font-size: 16px;
        }
    }
    
</style>


</head>
<body>

<form method='get' >
    <div class="input"><label>Start Date:</label> <input type="text" id="startDate" name="day" value="<?php echo $day; ?>" /></div>
    <div class="input"><label>End Date:</label> <input type="text" id="endDate" name="day" value="<?php echo $day; ?>" /></div>
        <button class="submit_button">GO</button>
        
        <span class="last_updated"></span></span>
</form>
    
    <div class="waiting"> <p>Please wait...  <i class="fa fa-spinner fa-spin"></i></p></div>
    
    
    <div class="results"></div>
    
        
</body>
<script type="text/javascript" >
    
    function timeConverter(timestamp) {
        var a = new Date(timestamp*1000);
        var months = ['January','Febuary','March','April','May','June','July','August','September','October','November','December'];
        var year = a.getFullYear();
        var month = months[a.getMonth()];
        var date = a.getDate();
        var hour = a.getHours();
        var min = a.getMinutes() < 10 ? '0' + a.getMinutes() : a.getMinutes(); 
        var sec = a.getSeconds() < 10 ? '0' + a.getSeconds() : a.getSeconds();
        var time = month + ' ' + date + ' ' + year + ' ' + hour + ':' + min + ':' + sec ;
        return time;
    }
    $(document).ready(function() {

        $("#startDate").datepicker({
            dateFormat: "yy-mm-dd"
        });
         $("#endDate").datepicker({
            dateFormat: "yy-mm-dd"
        }); 

        $('.submit_button').click(function(e){
            e.preventDefault();
            var startDate = $('#startDate').val();
            var endDate = $('#endDate').val();

            if (new Date(startDate) > new Date(endDate)) {
            console.log('error: end date connot be before start date.');
            $('#startDate').addClass('error');
            $('#endDate').addClass('error');
            return;
            }
            else {
                $('#startDate').removeClass('error');
                $('#endDate').removeClass('error');
            }
                
                $.ajax({
                    method: "GET",
                    beforeSend: function() {
                        $('.waiting').show();
                    },
                    url: "dashboard_retrieve.php",
                    data: {start: startDate, end: endDate}
                }).done(function ( data ) {
                        $('.waiting').hide();
                        $('.results').empty();
                        $('.last_updated').empty();
                        var timestamp = Math.floor(Date.now() / 1000);
                        var time = timeConverter(timestamp);
                        $('.last_updated').append('Last Updated: '+time);
                        $('.results').append(data);
                });
            

            
            
        });
    });
</script>
</html>